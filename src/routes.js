angular
    .module('app')
    .config(routesConfig);

/** @ngInject */
routesConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'lockerProvider', 'jwtOptionsProvider', 'cfpLoadingBarProvider'];

function routesConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, lockerProvider, jwtOptionsProvider, cfpLoadingBarProvider) {
    // $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/admin');
    // cfpLoadingBarProvider.includeBar = false;
    cfpLoadingBarProvider.latencyThreshold = 500;

    $stateProvider
        .state('app', {
            //component: 'app'
            abstract: true,
            url: '',
            templateUrl: 'app/templates/main.html'
        });
    lockerProvider.defaults({
        driver: 'local',
        namespace: 'dm',
        separator: '_',
        eventsEnabled: true,
        extend: {}
    });

    jwtOptionsProvider.config({
        whiteListedDomains: ['localhost', '158.69.202.20'],
        tokenGetter: ['options', 'locker', 'jwtHelper', 'API', 'sessionService', function(options, locker, jwtHelper, API, sessionService) {
            var jwt = locker.get('token');
            if (!options || options.url.substr(options.url.length - 5) === '.html') {
                return null;
            }
            if (jwt && jwtHelper.isTokenExpired(jwt)) {
                return API.get('refreshtoken', {
                    skipAuthorization: true,
                    headers: {
                        Authorization: 'Bearer ' + jwt
                    }
                })
            	.then(function(response) {
                        locker.put('token', response.data.data);
                        return response.data.data;
                    },
                    function(response) {
                        sessionService.cleanData();
                    });
            } else {
                return jwt;
            }
        }],
        unauthenticatedRedirector: ['sessionService', 'locker', function(sessionService, locker) {
            if (locker.get('token')) {
                sessionService.logout();
            }
        }]
    });
    $httpProvider.interceptors.push('jwtInterceptor');
}
