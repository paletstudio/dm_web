(function() {
    'use strict';

    angular.module('app', [
            'ui.router',
            'ui.bootstrap',
            'oitozero.ngSweetAlert',
            'angular-locker',
            'ui.select',
            'ngSanitize',
            'angular-jwt',
            'toastr',
            'angular-loading-bar'
        ])
        .run(run);

    run.$inject = ['$rootScope', '$state', 'sessionService', 'authManager', 'locker', 'jwtHelper'];

    function run($rootScope, $state, sessionService, authManager, locker, jwtHelper) {
        authManager.checkAuthOnRefresh();
        authManager.redirectWhenUnauthenticated();

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
            if (toState.url === '/login' && locker.get('token') && !jwtHelper.isTokenExpired(locker.get('token'))) {
                event.preventDefault();
                if (fromState.name === '') {
                    $state.go('app.admin');
                }
                return;
            }
            if (toState.requiresLogin && !sessionService.isLogged()){
                event.preventDefault();
                $state.go('login');
            }
        });
    }
})();
