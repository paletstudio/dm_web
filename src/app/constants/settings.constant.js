(function() {
    'use strict';

    angular
        .module('app')
        .constant('settings', settings());

        function settings() {
            return {
                /*inject-env*/
                'baseUrl': 'http://158.69.202.20/draught_master/services/public/api/',
    'origin': 1,
    'grade': {
      'star': 100,
      'approved': 70,
      'insufficient': 0
    }
                /*endinject*/
            };
        }
})();
