(function() {
	'use strict';

	angular
		.module('app')
		.factory('API', API);

	API.$inject = ['$http', '$q', 'settings'];

	/* @ngInject */
	function API($http, $q, settings) {
		var baseUrl = settings.baseUrl;

		function makeRequest(verb, uri, data, config) {
			verb = verb.toLowerCase();

			//start with the uri
			var url = baseUrl + uri;

			var httpArgs = [url];
			if (verb.match(/post|put/)) {
				httpArgs.push(data);
			}
			var headers = {
				headers: {
					'Content-Type': 'application/json'
				}
			};

			if (angular.isObject(config)) {
				angular.merge(headers, config);
			}

			httpArgs.push(headers);

			return $http[verb].apply(null, httpArgs)
				.catch(function(err) {
					if (err.status === -1) {
						console.log('Sin conexión a los servicios');
						return $q.reject(err);
					} /*else if (err.status === 401) {
						console.log('Unauthorized');
						return $q.reject('Unauthorized');
					}*/
					return $q.reject(err);
				});
		}

		var service = {
			get: function(uri, config) {
				return makeRequest('get', uri, null, config);
			},
			post: function(uri, data, config) {
				return makeRequest('post', uri, data, config);
			},
			put: function(uri, data, config) {
				return makeRequest('put', uri, data, config);
			},
			delete: function(uri) {
				return makeRequest('delete', uri);
			}
		};

		return service;
	}
})();
