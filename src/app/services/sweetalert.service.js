(function() {
	'use strict';

	angular
		.module('app')
		.service('sweetAlert', sweetAlert);

	sweetAlert.$inject = ['SweetAlert', '$q', '$window'];

	/* @ngInject */
	function sweetAlert(SweetAlert, $q, $window) {
		this.success = function(mensaje, titulo) {
			return $q(function(exito) {
				SweetAlert.swal({
					title: titulo || '',
					text: mensaje,
					type: 'success',
					html: true
				}, function() {
					$window.onkeydown = null;
					$window.onfocus = null;
					exito();
				});
			});
		};

		this.error = function(mensaje, titulo) {
			return $q(function(exito) {
				SweetAlert.swal({
					title: titulo || '',
					text: mensaje,
					type: 'error',
					html: true
				}, function() {
					exito();
				});
			});
		};

		this.warning = function(mensaje, titulo) {
			return $q(function(exito) {
				SweetAlert.swal({
					title: titulo || '',
					text: mensaje,
					type: 'warning',
					html: true
				}, function() {
					exito();
				});
			});
		};

		this.confirm = function(mensaje, titulo, closeOnConfirm) {
			return $q(function(exito, error) {
				SweetAlert.swal({
						title: titulo || '',
						text: mensaje,
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#DD6B55',
						confirmButtonText: 'OK',
						cancelButtonText: 'Cancelar',
						closeOnConfirm: closeOnConfirm === undefined ? true : closeOnConfirm,
						html: true
					},
					function(confirm) {
						if (confirm) {
							exito(confirm);
						} else {
							error(confirm);
						}
					});
			});
		};
	}
})();
