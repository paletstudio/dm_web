(function() {
	'use strict';

	angular
		.module('app')
		.service('programService', programService);

	programService.$inject = ['$rootScope', '$q', 'API'];

	/* @ngInject */
	function programService($rootScope, $q, API) {
		var me = this;
		me.currentCourse = null;
		me.currentBlock = null;
		me.currentModule = null;

		me.setCourse = function(id) {
			me.currentCourse = id;
		};

		me.setBlock = function(id) {
			me.currentBlock = id;
			return $q.resolve();
		};

		me.setModule = function(id) {
			me.currentModule = id;
		};

		me.getCourses = function() {

		};

		me.getCourse = function() {
			return API.get(getProgramUrl('course', true))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getBlocks = function(userId) {
			return API.get('course/1/block' + (userId ? ('?user_id=' + userId) : ''))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getBlock = function() {
			return API.get(getProgramUrl('block', true))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getModules = function(user_id) {
			var url = getProgramUrl('module');
			if (user_id) {
				url += '?user_id=' + user_id;
			}
			return API.get(url)
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getModule = function() {
			return API.get(getProgramUrl('module', true))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getContents = function(only_exams, user_id) {
			var url = getProgramUrl('content');
			if (only_exams && user_id) {
				url += '?only_exams=true&user_id=' + user_id;
			}
			return API.get(url)
				.then(function(res) {
					return res.data.data;
				});
		};

		me.fireReloadEvent = function() {
			return $rootScope.$broadcast('program.reload');
		};

		me.onReloadEvent = function(fn) {
			return $rootScope.$on('program.reload', fn);
		};

		me.sendInvitation = sendInvitation;
		me.sendCertificate = sendCertificate;

		function getProgramUrl(level, detail) {
			if (level === 'course') {
				return 'course' + (detail ? '/' + me.currentCourse : '');
			} else if (level === 'block') {
				return getProgramUrl('course', true) + '/block' + (detail ? '/' + me.currentBlock : '');
			} else if (level === 'module') {
				return getProgramUrl('block', true) + '/module' + (detail ? '/' + me.currentModule : '');
			} else if (level === 'content') {
				return getProgramUrl('module', true) + '/content' + (detail ? '/' + me.currentContent : '');
			}
		}

		function sendInvitation(user_id) {
			return API.post('user/' + user_id + '/sendinvitation', {})
			.then(function(res) {
				return $q.resolve(res.data);
			})
			.catch(function(err) {
				return $q.reject(err);
			});
		}

		function sendCertificate(user_id) {
			return API.post('course/1/certificate/user/' + user_id, {})
			.then(function(res) {
				return $q.resolve(res.data);
			})
			.catch(function(err) {
				return $q.reject(err);
			});
		}
	}
})();
