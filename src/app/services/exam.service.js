(function() {
	'use strict';

	angular
		.module('app')
		.service('examService', examService);

	examService.$inject = ['API', 'programService'];

	/* @ngInject */
	function examService(API, programService) {
		var me = this;

		/**
		 * Evaluate a single answer
		 * @param  {object} answer Answer object to evaluate.
		 * @return {Promise}
		 */
		me.evaluateAnswer = function(answer) {
			return API.post('answer/' + answer.answer.id, answer)
				.then(function(res) {
					return res.data;
				})
				.catch(function(err) {
					return err;
				});
		};

		/**
		 * Send a complete exam and evaluate it.
		 * @param  {int} exam_id      Exam Attempt id.
		 * @param  {int} grade        Grade to assign.
		 * @param  {array} exam_answers Exam answers.
		 * @return {Promise}
		 */
		me.evaluateExam = function(exam_id, grade, exam_answers) {
			return API.post('exam/' + exam_id + '/grade?block_id=' + programService.currentBlock + '&module_id=' + programService.currentModule, {grade: grade, exam: exam_answers})
			.then(function(res) {
				return res.data;
			})
			.catch(function(err) {
				return err;
			})
			.finally(function() {
				programService.fireReloadEvent();
			});
		};

		/**
		 * Get pending exams
		 *
		 * This function get all pending user exams.
		 * @return {Promise}
		 */
		me.getPendingExams = function() {
			return API.get('exam/pending')
			.then(function(res) {
				return res.data;
			})
			.catch(function(err){
				return err;
			});
		};
	}
})();
