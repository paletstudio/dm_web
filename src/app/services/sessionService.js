(function() {
    'use strict';

    angular
        .module('app')
        .service('sessionService', sessionService);

    sessionService.$inject = ['API', 'locker', '$state', 'jwtHelper', 'toastr', 'settings'];

    /* @ngInject */
    function sessionService(API, locker, $state, jwtHelper, toastr, settings) {
        var me = this;

        me.login = function(username, password) {
            return API.post('login', {
                    username: username,
                    password: password,
                    appId: settings.appId
                })
                .then(function(res) {
                    var token = res.data.data;
                    var user = jwtHelper.decodeToken(token);
                    locker.put('token', token);
                    locker.put('user', user.user);
                    $state.go('app.admin');
                })
                .catch(function(err) {
                    toastr.error(err.data.msg, 'Error');
                    return err;
                });
        };

        me.logout = function() {
            return API.post('logout')
                .then(function(res) {
                    locker.forget('token');
                    locker.forget('user');
                    toastr.success('Log out exitoso');
                    $state.go('login');
                })
                .catch(function(err) {
                    return err;
                });
        };

        me.cleanData = function() {
            locker.forget('user');
            locker.forget('token');
            $state.go('login');
        };

        me.isLogged = function() {
            return !!locker.get('token');
        };
    }
})();
