(function() {
    'use strict';

    angular.module('app')
        .directive('evTable', evTable);

    function evTable() {
        return {
            template: evTableTemplate(),
            restrict: 'A',
            scope: {
                collection: '=',
                filter: '=',
                itemsPerPage: '@',
                showAllItems: '='
            },
            controller: EvTableCtrl,
            controllerAs: 'vm',
            bindToController: true,
            transclude: true,
            replace: true,
            link: function postLink(scope, elem, attrs, vm) {
                scope.$watch('vm.collection', function(newValue) {
                    if (newValue !== undefined) {
                        vm.paginate();
                    }
                });

                scope.$watch('vm.filter', function(newValue) {
                    vm.puedeFiltrar = false;
                    if (angular.isUndefined(newValue) || newValue === true) {
                        vm.puedeFiltrar = true;
                    }
                });

                scope.$watch('vm.itemsPerPage', function(newValue) {
                    vm.pagination.itemsPerPage = 10;
                    if (angular.isDefined(newValue) && angular.isNumber(parseInt(newValue))) {
                        vm.pagination.itemsPerPage = parseInt(newValue);
                    }
                });

                scope.$watch('vm.showAllItems', function(newValue) {
                    if (angular.isDefined(newValue) && newValue === false) {
                        vm.pagination.itemsPerPageOptions.splice(4, 1);
                    }
                });

            }
        };
    }

    EvTableCtrl.$inject = ['$scope', '$filter'];

    function EvTableCtrl($scope, $filter) {
        var vm = this;

        vm.pagination = {};
        vm.pagination.page = 1;
        vm.pagination.itemsPerPageOptions = [{
            'id': 1,
            'name': '10',
            'value': 10
        }, {
            'id': 2,
            'name': '25',
            'value': 25
        }, {
            'id': 3,
            'name': '50',
            'value': 50
        }, {
            'id': 4,
            'name': '100',
            'value': 100
        }, {
            'id': 5,
            'name': 'Todos',
            'value': 10000000000
        }];
        vm.pagination.itemsPerPage = vm.itemsPerPage || vm.pagination.itemsPerPageOptions[0].value;
        vm.paginate = paginate;

        //Esta funcion pagina y filtra cada se llama
        function paginate() {
            //rows es la coleccion de elementos
            var rows = vm.collection || [];
            //filter rows
            var filteredRows = [];
            filteredRows = $filter('filter')(rows, vm.pagination.search || '');
            vm.pagination.totalRows = filteredRows.length;
            if (vm.pagination.itemsPerPage !== 'Todos') {
                if (filteredRows.length !== rows.length) {
                    vm.pagination.page = 1;
                }
                var start = (vm.pagination.page - 1) * vm.pagination.itemsPerPage;
                var end = start + vm.pagination.itemsPerPage;
                filteredRows = filteredRows.slice(start, end);
            }

            $scope.$parent.$rows = filteredRows;
        }
    }

    function evTableTemplate() {
        return [
            '<div class="row">',
            '    <div class="col-md-12">',
            '        <div class="row form-inline" style="padding-bottom: 10px" ng-if="vm.puedeFiltrar">',
            '            <div class="col-md-3">',
            '                <div class="form-group">',
            '                    <label for="" class="label-control">Buscar</label>',
            '                    <input type="text" ng-model="vm.pagination.search" ng-change="vm.paginate()" class="form-control" placeholder="Busca un nombre...">',
            '                </div>',
            '            </div>',
            '            <div class="col-md-4 col-md-offset-5">',
            '                <div class="form-group pull-right">',
            '                    <select name="" id="" class="form-control" ng-model="vm.pagination.itemsPerPage" ng-options="o.value as o.name for o in vm.pagination.itemsPerPageOptions" ng-change="vm.paginate()"></select>',
            '                    <label for="" class="label-control">registros de un total de {{vm.pagination.totalRows}}</label>',
            '                </div>',
            '            </div>',
            '        </div>',
            '        <table id="evTable" ng-transclude="" style="margin-bottom: 5px" class="table table-striped table-condensed table-responsive" wt-responsive-table>',
            '        </table>',
            '        <div class="text-center" ng-show="(vm.pagination.totalRows / vm.pagination.itemsPerPage) > 1">',
            '            <ul uib-pagination total-items="vm.pagination.totalRows" items-per-page="vm.pagination.itemsPerPage" ng-change="vm.paginate()" ng-model="vm.pagination.page" max-size="5" class="pagination-sm" boundary-links="true" force-ellipses="true" first-text="Primero" last-text="&Uacute;ltimo" previous-text="Ant." next-text="Sig."></ul>',
            '        </div>',
            '    </div>',
            '</div>'
        ].join('');
    }
})();
