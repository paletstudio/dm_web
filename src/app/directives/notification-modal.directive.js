(function() {
    'use strict';

    angular
        .module('app')
        .directive('notificationModal', notificationModal);

    /* @ngInject */
    function notificationModal() {
        var directive = {
            restrict: 'A',
            scope: {
                user: '='
            },
            controller: NotificationModalController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

    NotificationModalController.$inject = ['$uibModal', '$element', 'sweetAlert'];

    /* @ngInject */
    function NotificationModalController($uibModal, $element, sweetAlert) {
        var vm = this;
        $element.on('click', modal);

        function modal() {
			return $uibModal.open({
                templateUrl: 'app/templates/notification-modal.html',
				controller: NotificationController,
				controllerAs: 'vm',
				size: 'lg',
                resolve: {
                    _user: vm.user || null
                }
			}).result.then(function(res) {
				sweetAlert.success(res.msg, 'Éxito');
			});
		}
    }

    NotificationController.$inject = ['$uibModalInstance', '_user', 'notificationService'];
    function NotificationController($uibModalInstance, _user, notificationService) {
        var vm = this;
        vm.close = close;
        vm.notification = notificationService.init();
        vm.sendNotification = sendNotification;
        if (_user) {
            vm.user = _user;
            vm.notification.users.push(_user.id);
        }

        function close() {
            $uibModalInstance.dismiss();
        }

        function sendNotification() {
            notificationService.sendNotification(vm.notification)
            .then(function(res) {
                $uibModalInstance.close(res);
            });
        }
    }
})();
