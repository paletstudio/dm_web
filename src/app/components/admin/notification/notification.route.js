(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'app/components/admin/notification/';
        return [{
            state: 'app.admin.notification',
            config: {
                url: '/notificacion',
                templateUrl: baseUrl + 'notification.html',
                controller: 'AdminNotificationController',
                controllerAs: 'vm',
                resolve: {
                    _users: ['userService', function(userService) {
                        return userService.getUsers();
                    }]
                },
                requiresLogin: true,
            }
        }];
    }
})();
