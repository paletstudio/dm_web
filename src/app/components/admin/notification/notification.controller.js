(function() {
    'use strict';

    angular
        .module('app')
        .controller('AdminNotificationController', AdminNotificationController);

    AdminNotificationController.$inject = ['_users', 'notificationService', 'sweetAlert'];

    /* @ngInject */
    function AdminNotificationController(_users, notificationService, sweetAlert) {
        var vm = this;
        vm.users = _users.data;
        clean();
        vm.selectAll = selectAll;
        vm.sendNotification = sendNotification;

        function selectAll() {
            vm.notification.users = [];
        }
        function clean() {
            vm.notification = {
                users: [],
                msg: ''
            };
            vm.all = false;
        }

        function sendNotification() {
            notificationService.sendNotification(vm.notification)
            .then(function(res) {
                clean();
                sweetAlert.success(res.msg, 'Éxito');
            })
            .catch(function(err) {
                sweetAlert.error(err.msg, 'Error');
            });
        }

    }
})();
