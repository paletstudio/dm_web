(function() {
    'use strict';

    angular
        .module('app')
        .service('notificationService', notificationService);

    notificationService.$inject = ['API', '$q', 'toastr'];

    /* @ngInject */
    function notificationService(API, $q, toastr) {
        var me = this;

        me.init = function() {
            return {
                users: [],
                msg: ''
            };
        };

        me.sendNotification = function(notification) {
            return API.post('notification', notification)
            .then(function(res) {
                return $q.resolve(res.data);
            })
            .catch(function(err) {
                return $q.reject(err);
            });
        };
    }
})();
