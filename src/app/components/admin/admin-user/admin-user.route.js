(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'app/components/admin/admin-user/';
        return [{
            state: 'app.admin.user',
            config: {
                url: '/user',
                templateUrl: baseUrl + 'admin-user.html',
                controller: 'AdminUserController',
                controllerAs: 'vm',
                requiresLogin: true,
            }
        }];
    }
})();
