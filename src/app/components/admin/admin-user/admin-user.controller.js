(function() {
    'use strict';

    angular
        .module('app')
        .controller('AdminUserController', AdminUserController);

    AdminUserController.$inject = ['userService', 'sweetAlert'];

    /* @ngInject */
    function AdminUserController(userService, sweetAlert) {
        var vm = this;
        vm.user = userService.init();
        vm.save = save;
        vm.clean = clean;

        function save() {
            vm.user.username = vm.user.email;
            userService.addUser(vm.user)
            .then(function(res) {
                clean();
                sweetAlert.success(res.msg, 'Éxito');
            })
            .catch(function(err) {
                sweetAlert.error(err.data.exception, 'Error');
            });
        }

        function clean() {
            vm.user = userService.init();
        }
    }
})();
