(function() {
    'use strict';

    angular
        .module('app')
        .controller('AdminTableController', AdminTableController);

    AdminTableController.$inject = ['$uibModal', 'settings', '_users', 'locker', 'sweetAlert', 'userService', 'toastr'];

    /* @ngInject */
    function AdminTableController($uibModal, settings, _users, locker, sweetAlert, userService, toastr) {
        var vm = this;
        vm.notificationModal = notificationModal;
        vm.users = _users.data;
        vm.getPdfUrl = getPdfUrl;
		vm.deleteUser = deleteUser;

        function notificationModal(user_id) {
            return $uibModal.open({
                    templateUrl: 'app/components/admin/admin-table/modals/notification-modal/notification-modal.html',
                    controller: 'NotificationModalController',
                    controllerAs: 'vm',
                    resolve: {
                        _user: user_id || null
                    }
                })
                .result
                .then(function(res) {
                    //Resultado
                })
                .catch(function(err) {
                    // Rechazado
                });
        }

        function getPdfUrl(user) {
            return settings.baseUrl + 'course/1/certificate/user/' + user + '?token=' + locker.get('token');
        }

		function deleteUser(user) {
			sweetAlert.confirm('¿Desea eliminar al usuario ' + user.name + ' ' + user.lastname + '?', 'Eliminar usuario')
			.then(function(res) {
				userService.deleteUser(user.id)
				.then(function(res) {
					var index = vm.users.indexOf(user);
					var users = angular.copy(vm.users);
					users.splice(index, 1);
					vm.users = users;
					toastr.success(res.msg, 'Éxito');
				})
				.catch(function(err) {
					toastr.error(err.data.msg, 'Error');
				});
			});
		}
    }
})();
