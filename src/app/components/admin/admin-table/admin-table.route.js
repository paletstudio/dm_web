(function() {
	'use strict';

	angular
		.module('app')
		.run(appRun);

	appRun.$inject = ['routerHelper'];
	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());
	}

	function getStates() {var baseUrl = 'app/components/admin/admin-table/';

		return [{
			state: 'app.admin.table',
			config: {
				url: '/table',
				templateUrl: baseUrl + 'admin-table.html',
				controller: 'AdminTableController',
				controllerAs: 'vm',
				resolve: {
					_users: ['userService', function(userService) {
						return userService.getUsers(true);
						// return null;
					}]
				},
                requiresLogin: true,
			}
		}];
	}
})();
