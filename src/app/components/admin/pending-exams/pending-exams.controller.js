(function() {
    'use strict';

    angular
        .module('app')
        .controller('PendingExamsController', PendingExamsController);

    PendingExamsController.$inject = ['_exams'];

    /* @ngInject */
    function PendingExamsController(_exams) {
        var vm = this;
        vm.exams = _exams;
        console.log(vm.exams);

    }
})();
