(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'app/components/admin/pending-exams/';
        return [{
            state: 'app.admin.exams',
            config: {
                url: '/exams',
                templateUrl: baseUrl + 'pending-exams.html',
                controller: 'PendingExamsController',
                controllerAs: 'vm',
                resolve: {
                    _exams: ['examService', function(examService) {
                        return examService.getPendingExams();
                    }]
                },
                requiresLogin: true,
            }
        }];
    }
})();
