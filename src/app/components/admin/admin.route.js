(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates(service) {
        var baseUrl = 'app/components/admin/';

        return [{
            state: 'app.admin',
            config: {
                url: '/admin',
                templateUrl: baseUrl + 'admin.html',
                controller: 'AdminController',
                controllerAs: 'vm',
                requiresLogin: true
                // data: {
                // }
            }
        }];
    }
})();
