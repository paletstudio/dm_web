(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'app/components/login/';

        return [{
            state: 'login',
            config: {
                url: '/login',
                templateUrl: baseUrl + 'login.html',
                controller: 'LoginController as vm'
            }
        }];
    }
})();
