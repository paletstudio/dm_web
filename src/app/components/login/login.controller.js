(function() {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['sessionService'];

    /* @ngInject */
    function LoginController(sessionService) {
        var vm = this;
        vm.login = login;

        function login(){
            sessionService.login(vm.username, vm.password);
        }
    }
})();
