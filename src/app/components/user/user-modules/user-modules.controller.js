(function() {
    'use strict';

    angular
        .module('app')
        .controller('UserModuleController', UserModuleController);

    UserModuleController.$inject = ['$stateParams', '_modules', '_block', 'programService'];

    /* @ngInject */
    function UserModuleController($stateParams, _modules, _block, programService) {
        var vm = this;
        vm.modules = _modules;
		vm.block = _block;
		vm.reload = reload;

        activate();

        function activate() {

        }

		function reload() {
			programService.currentBlock = $stateParams.blockId;
			programService.getModules($stateParams.id)
			.then(function(res) {
				vm.modules = res;
			});
			programService.fireReloadEvent();
		}
    }
})();
