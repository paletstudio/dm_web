(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'app/components/user/user-modules/';
        return [{
            state: 'app.user.module',
            config: {
                url: '/block/{blockId:int}/module',
                templateUrl: baseUrl + 'user-module.html',
                controller: 'UserModuleController',
                controllerAs: 'vm',
                resolve: {
                    _modules: ['$stateParams', 'programService', function($stateParams, programService) {
                        programService.currentBlock = $stateParams.blockId;
                        return programService.getModules($stateParams.id);
                    }],
					_block: ['$stateParams', 'programService', function($stateParams, programService) {
						return programService.getBlock();
					}]
                },
                requiresLogin: true,
            }
        }];
    }
})();
