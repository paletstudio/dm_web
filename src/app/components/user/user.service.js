(function() {
    'use strict';

    angular
        .module('app')
        .service('userService', userService);

    userService.$inject = ['API', '$q'];

    /* @ngInject */
    function userService(API, $q) {
        var me = this;
        var path = 'user';

        /**
         * Initializes a user
         * @return {object} User
         */
        me.init = function() {
            return {
                name: '',
                lastname: '',
                email: '',
                username: '',
                city: '',
                state: '',
                profile_id: ''
            };
        };

        /**
         * Register a user
         * @param {object} user User to register
         * @return {promise}
         */
        me.addUser = function(user) {
            return API.post('register', user)
                .then(function(res) {
                    return $q.resolve(res.data);
                })
                .catch(function(err) {
                    return $q.reject(err);
                });
        };

        /**
         * Retrieves a single user.
         * @param  {int} id User id
         * @return {promise}
         */
        me.getUser = function(id) {
            return API.get(path + '/' + id)
                .then(function(res) {
                    return res.data;
                })
                .catch(function(err) {
                    return err;
                });
        };

        /**
         * Get list of users.
         * @return {promise}
         */
        me.getUsers = function(info) {
            return API.get(path + (info ? '?info=true' : ''))
                .then(function(res) {
                    return res.data;
                })
                .catch(function(err) {
                    return err;
                });
        };

		me.deleteUser = function(id) {
			return API.delete(path + '/' + id)
			.then(function(res) {
				return $q.resolve(res.data);
			})
			.catch(function(err) {
				return $q.reject(err);
			});
		};
    }
})();
