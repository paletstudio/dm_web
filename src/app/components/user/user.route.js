(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'app/components/user/';

        return [{
            state: 'app.user',
            config: {
                url: '/user/{id:int}',
                templateUrl: baseUrl + 'user.html',
                controller: 'UserController',
                controllerAs: 'vm',
                resolve: {
                    _blocks: ['$stateParams', 'programService', function($stateParams, programService) {
                        programService.currentCourse = 1;
                        return programService.getBlocks($stateParams.id);
                    }],
                    _user: ['$stateParams', 'userService', function($stateParams, userService) {
                        return userService.getUser($stateParams.id);
                    }]
                },
                requiresLogin: true,
            }
        }, {
            state: 'app.user.default',
            config: {
                url: '',
                templateUrl: baseUrl + 'user-default.tml',
                controller: 'UserController',
                controllerAs: 'vm',
                requiresLogin: true,
            }
        }];
    }
})();
