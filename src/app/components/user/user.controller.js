(function() {
    'use strict';

    angular
        .module('app')
        .controller('UserController', UserController);

    UserController.$inject = ['$scope', '$state', '_user', '_blocks', 'settings', 'programService', 'sweetAlert'];

    /* @ngInject */
    function UserController($scope, $state, _user, _blocks, settings, programService, sweetAlert) {
        var vm = this;
		var unregisterReloadEvent;
        vm.blocks = _blocks;
        vm.user = _user.data;
        vm.profileUrl = function() {
            return settings.baseUrl + 'user/' + vm.user.id + '/picture?' + moment();
        };
        vm.sendInvitation = sendInvitation;
        vm.sendCertificate = sendCertificate;

        function sendInvitation() {
            sweetAlert.confirm('¿Desea enviar la invitación a certificación a\r\n' + vm.user.name + ' ' + vm.user.lastname + '?', '¿Está seguro?', true)
            .then(function(res) {
                programService.sendInvitation(vm.user.id)
                .then(function(res) {
                    sweetAlert.success(res.msg, 'Éxito');
                })
                .catch(function(err) {
                    sweetAlert.err('Ocurrió un error al enviar certificado. \r\nEnviarlo más tarde.', 'Error');
                });
            });
        }

        function sendCertificate() {
            sweetAlert.confirm('¿Desea enviar el certificado de Draught Master a \r\n' + vm.user.name + ' ' + vm.user.lastname + '?', '¿Está seguro?', true)
            .then(function(res) {
                programService.sendCertificate(vm.user.id)
                .then(function(res) {
                    sweetAlert.success(res.msg, 'Éxito');
                })
                .catch(function(err) {
                    sweetAlert.err(err.data.msg, 'Error');
                });
            });
        }

		unregisterReloadEvent = programService.onReloadEvent(function() {
			programService.getBlocks($state.params.id)
			.then(function(res) {
				vm.blocks = res;
			});
		});

		$scope.$on('$destroy', function(){
			unregisterReloadEvent();
		});
    }
})();
