(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'app/components/user/user-content/';
        return [{
            state: 'app.user.module.content',
            config: {
                url: '/{moduleId:int}',
                views: {
                    '@app.user': {
                        templateUrl: baseUrl + 'user-content.html',
                        controller: 'UserContentController',
                        controllerAs: 'vm',
                        resolve: {
                            _questions: ['$stateParams', 'programService', function($stateParams, programService) {
                                programService.currentModule = $stateParams.moduleId;
                                return programService.getContents(true, $stateParams.id);
                            }]
                        },
                        requiresLogin: true,
                    }
                }
            }
        }];
    }
})();
