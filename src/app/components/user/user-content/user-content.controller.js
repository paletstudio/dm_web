(function() {
	'use strict';

	angular
		.module('app')
		.controller('UserContentController', UserContentController);

	UserContentController.$inject = ['$uibModal', '_questions', 'examService', 'sweetAlert'];

	/* @ngInject */
	function UserContentController($uibModal, _questions, examService, sweetAlert) {
		var vm = this;
		vm.questions = _questions;
		vm.self_grade = isSelfRated();
		if (vm.self_grade) {
			vm.evaluated = true;
		} else {
			vm.evaluated = vm.questions[0].exam[0].answer.grade ? true : false;
		}

		vm.textAnswer = textAnswer;
		if ( vm.questions[0].exam.length > 0 && vm.questions[0].exam[0].answer) {
			vm.exam_attempt_id = vm.questions[0].exam[0].answer.id;
		}
		vm.compare = compare;
		vm.fileModal = fileModal;
		vm.evaluateModal = evaluateModal;
		vm.grade = null;
		vm.setGrade = setGrade;
		vm.evaluateExam = evaluateExam;

		function compare(object) {
			return object.answer.answer === object.correct_answer_id;
		}

		function fileModal(file) {
			return $uibModal.open({
				templateUrl: 'app/components/user/user-content/file-modal.html',
				controller: 'FileModalController',
				controllerAs: 'vm',
				size: 'lg',
				resolve: {
					_file: function() {
						return file;
					}
				}
			}).result.then(function(res) {
				//Resultado
			});
		}

		function evaluateModal(answer) {
			return $uibModal.open({
					templateUrl: 'app/components/user/user-content/evaluate-modal.html',
					controller: 'EvaluateModalController',
					controllerAs: 'vm',
					size: 'lg',
					resolve: {
						_answer: function() {
							return answer;
						}
					}
				}).result
				.then(function(res) {
					//answer.grade = res.grade;
					console.log(vm.questions[0].exam);
				})
				.catch(function(err) {
					//console.log(err);
				});
		}

		function setGrade(grade) {
			vm.grade = grade;
		}

		function calculateExamGrade() {
			var grade = 0;
			vm.questions[0].exam.forEach(function(q) {
				if (q.grade) {
					grade += q.grade;
				}
			});
			return grade;
		}

		function evaluateExam() {
			var grade = calculateExamGrade();
			examService.evaluateExam(vm.exam_attempt_id, grade, vm.questions[0].exam)
			.then(function(res) {
				vm.evaluated = true;
				sweetAlert.success(res.msg, 'Éxito');
			});
		}

		function textAnswer(obj) {
			if (Number(obj.answer.answer) === 1) {
				return obj.answer1;
			}
			if (Number(obj.answer.answer) === 2) {
				return obj.answer2;
			}
			if (Number(obj.answer.answer) === 3) {
				return obj.answer3;
			}
			if (Number(obj.answer.answer) === 4) {
				return obj.answer4;
			}
		}

		function isSelfRated() {
			var res = true;
			vm.questions[0].exam.forEach(function(q) {
				if (q.exam_answer_type_id !== 2) {
					res = false;
				}
			});
			return res;
		}
	}
})();
