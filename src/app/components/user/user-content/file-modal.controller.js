(function() {
    'use strict';

    angular
        .module('app')
        .controller('FileModalController', FileModalController);

    FileModalController.$inject = ['$state', '$uibModalInstance', 'settings', '_file', 'locker'];

    /* @ngInject */
    function FileModalController($state, $uibModalInstance, settings, _file, locker) {
        var vm = this;
        var baseUrl = settings.baseUrl;
        vm.close = close;
        vm.type = _file.exam_answer_type_id;
        if ( vm.type === 3) {
            vm.src = _file.answer.answer.replace('course/', 'image/');
        } else {
            vm.src = _file.answer.answer.replace('course/', 'video/');
        }
        vm.url = baseUrl + vm.src + '?token=' + locker.get('token');

        function close() {
            $uibModalInstance.close(vm.grade);
        }
    }
})();
