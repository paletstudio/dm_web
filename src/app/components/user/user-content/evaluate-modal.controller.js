(function() {
	'use strict';

	angular
		.module('app')
		.controller('EvaluateModalController', EvaluateModalController);

	EvaluateModalController.$inject = ['$state', '$uibModalInstance', 'settings', '_answer', 'examService'];

	/* @ngInject */
	function EvaluateModalController($state, $uibModalInstance, settings, _answer, examService) {
		var vm = this;
		vm.close = close;
		vm.setGrade = setGrade;
		vm.evaluateAnswer = evaluateAnswer;
		vm.answer = _answer;

		function close() {
			$uibModalInstance.dismiss('no reason');
		}

		function setGrade(grade) {
			vm.answer.grade = grade;
		}

		function evaluateAnswer() {
			$uibModalInstance.close(vm.answer);
		}

	}
})();
