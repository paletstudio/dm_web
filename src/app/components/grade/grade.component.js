(function() {
    'use strict';

    angular
        .module('app')
        .component('grade', grade());

    /* @ngInject */
    function grade() {
        var component = {
            templateUrl: 'app/components/grade/grade.html',
            controller: GradeController,
            controllerAs: 'vm',
            bindings: {
                value: '=',
                showFull: '='
            }
        };

        return component;
    }

    GradeController.$inject = ['settings'];

    /* @ngInject */
    function GradeController(settings) {
        var vm = this;

        vm.$onInit = function() {
            if (Number(vm.value) === settings.grade.star) {
                vm.case = 1;
            } else if (Number(vm.value) > settings.grade.approved) {
                vm.case = 2;
            } else if (Number(vm.value) > settings.grade.insufficient) {
                vm.case = 3;
            } else if (Number(vm.value) < settings.grade.insufficient) {
                vm.case = 4;
            }
        };

    }
})();
