(function() {
    'use strict';

    angular
        .module('app')
        .directive('header', header);

    /* @ngInject */
    function header() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/header/header.html',
            scope: {},
            controller: Controller,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

    Controller.$inject = ['sessionService'];

    /* @ngInject */
    function Controller(sessionService) {
        var vm = this;
        vm.logout = logout;

        function logout() {
            sessionService.logout();
        }
    }
})();
